import { NewsService } from './../../services/news.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.page.html',
  styleUrls: ['./news-details.page.scss'],
})
export class NewsDetailsPage implements OnInit {

  information = null;
  aaa = null;

  /**
   * Constructor of our details page
   * @param activatedRoute Information about the route we are on
   * @param newsService The movie Service to get data
   */
  constructor(private activatedRoute: ActivatedRoute, private newsService: NewsService) { }

  ngOnInit() {
    // Get the ID that was passed with the URL
    let id = this.activatedRoute.snapshot.paramMap.get('id');

    console.log(id);

    // Get the information from the API
    this.information = this.newsService.getDetails(id).subscribe(result => {
      this.information = result;
    });

    console.log(this.information.title);

  }

  openWebsite() {
    window.open(this.information.Website, '_blank');
  }
}