import { NewsService, SearchType } from 'src/app/services/news.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-movies',
  templateUrl: './news-list.page.html',
  styleUrls: ['./news-list.page.scss'],
})
export class NewsListPage implements OnInit {

  results: Observable<any>;
  searchTerm: string = '';
  searchTerm2: string = '';
  type2: string = '';
  type: string = '';

  /**
   * Constructor of our first page
   * @param movieService The movie Service to get data
   */
  constructor(private newsService: NewsService) { this.searchChanged();}

  ngOnInit() { }

  searchChanged() {

    

    this.searchTerm2 = this.searchTerm;
    this.type2 = this.type;

    if(this.searchTerm.length == 0) {
      this.searchTerm2 = '';
    }
    if(this.type.length == 0) {
      this.type2 = 'all';
    }

    // Call our service function which returns an Observable
    this.results = this.newsService.searchData(this.searchTerm2, this.type2);
  }
}
