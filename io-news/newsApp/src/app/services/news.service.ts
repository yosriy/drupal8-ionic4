import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// Typescript custom enum for search types (optional)
export enum SearchType {
  all = '',
  sport = 'sport',
  art = 'art',
  music = 'music'
}

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  url = 'http://iotest.yy/api/v1/news/';


  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor(private http: HttpClient) { }

  /**
   * Get data from the OmdbApi
   * map the result to return only the results that we need
   *
   * @param {string} title Search Term
   * @param {SearchType} type movie, series, episode or empty
   *  @returns Observable with the search results
   */
  searchData(title: string, type: string): Observable<any> {
    return this.http.get(`${this.url}type/${type}?title=${title}`).pipe(
      map(results => results)
    );
  }

  /**
   * Get the detailed information for an ID using the "i" parameter
   *
   * @param {string} id imdbID to retrieve information
   *  @returns Observable with detailed information
   */
  getDetails(id) {
    console.log(`${this.url}${id}`);
    //return this.http.get(`${this.url}${id}`);

    return this.http.get(`${this.url}${id}`).pipe(
      map(results => results)
    );
  }
}
