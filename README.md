**Simple Excercise for loading content from Drupal Website into an ionic mobile app**

Just follow the Steps Hereunder:

---

## Drupal Website

You’ll start by creating the content to load from Drupal.

1. Install a Drupal 8 website locally (MAMP, WAMP, XAMPP...)
2. Add a new content type: News (Title [default], Body [default], Image [image file], News Type [Entity ref: Taxonomy]) 
![News Content Type](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io2.1.png)
![News Content Type](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io2.2.png)
3. Create 5 news (using Devel Generate or manually)
4. Install / Enable the Rest Module: https://www.drupal.org/project/restui
5. Add a new View: News API (Rest Display : Provide a REST export)
![News Rest View](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io5.png)
  
6. For the fist Display: News List: [path: /api/v1/news] ![Views List](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io6.png)
	i.   Select the Settings Format: json ![Views Format](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io6.1.png)
	ii.  Select the Show Format : Fields ![Views Fields](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io6.2.png)
	iii. Add the fields: ID, Title (no link), Image (URL), News Type (no link) and Date (created /Authored on)  
	iv.  In "PATH SETTINGS" select "Unrestricted" as Access ![Views Restriction](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io6.1.png)
7. For the second Display: News Search: [path: /api/v1/news/type/%] ![Views Restriction](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io7.png)
	i.   Select the Settings Format: json  ![Views Format](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io6.1.png)
	ii.  Select the Show Format : Fields
	iii. Add the fields: ID, Title (no link), Image (URL), News Type (no link) and Date (created /Authored on)  
	iv.  In "PATH SETTINGS" select "Unrestricted" as Access ![Views Restriction](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io6.1.png)
	v.   In "FILTER CRITERIA" add "Title" as "Exposed Filter" ![Exposed Filter](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io7.5.png)
	vi.  Open "ADVANCED" in "CONTEXTUAL FILTERS" select in "WHEN THE FILTER VALUE IS NOT AVAILABLE", "Provide default value":
	     "Raw value from URL" and "Path Component" = 5 ![Contextual Filter](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io7.6.png)
8. For the second Display: News Details: [path: /api/v1/news/%] ![Views Restriction](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io8.png)
	i.   Select the Settings Format: json  ![Views Format](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io6.1.png)
	ii.  Select the Show Format : Fields
	iii. Add the fields: ID, Title (no link), Image (URL), News Type (no link) and Date (created /Authored on)  
	iv.  In "PATH SETTINGS" select "Unrestricted" as Access ![Views Restriction](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io6.1.png)
	v.   Open "ADVANCED" in "CONTEXTUAL FILTERS" select in "WHEN THE FILTER VALUE IS NOT AVAILABLE", "Provide default value":
	     "Raw value from URL" and "Path Component" = 4 ![Contextual Filter](https://bitbucket.org/yosriy/drupal8-ionic4/raw/ecaa02c2bfff38e7708fdf49c85e94d1feadd3eb/imgs/io8.5.png)
---


## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).